package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"os"
	"time"
)

func main() {
	err := godotenv.Load()
	// .env file is required
	now := time.Now()
	if err != nil {
		panic(err)
	}
	c := client.NewWarehouseClient(fmt.Sprintf("localhost:%s", os.Getenv("WAREHOUSE_RPC_PORT")), 1*time.Second)
	dashboard := c.GetAllDashboard()
	fmt.Printf("dashboard: %+v\n", dashboard)
	fmt.Printf("time: %v\n", time.Since(now))
}
