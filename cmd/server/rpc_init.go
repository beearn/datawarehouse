package main

import (
	"gitlab.com/beearn/datawarehouse/modules/warehouse"
	"gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/datawarehouse/wrpc/server"
	"google.golang.org/grpc"
	"log"
	"net"
)

func initRpc(warehouse *warehouse.Warehouse) {
	lis, err := net.Listen("tcp", ":4080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer(grpc.MaxRecvMsgSize(1024*1024*1024), grpc.MaxSendMsgSize(1024*1024*1024))
	wrpc.RegisterWarehouseServer(s, server.NewDataWarehouseServer(warehouse))
	log.Printf("grpc server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
