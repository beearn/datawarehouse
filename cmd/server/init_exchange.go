package main

import (
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
	"net"
	"os"
	"strconv"
	"strings"
)

func ExchangeInit() *adapter.Binance {
	var (
		//Binance
		binanceAPIKey    = os.Getenv("BINANCE_API_KEY")
		binanceSecretKey = os.Getenv("BINANCE_SECRET_KEY")
	)
	// Exchange functionality initialization
	serversRaw := os.Getenv("BINANCE_EXCHANGE_CLIENT_RPC_SERVERS")
	servers := strings.Split(serversRaw, ",")

	for i, server := range servers {
		if !validateAddress(server) {
			panic("Invalid server address: " + server)
		}
		servers[i] = strings.TrimSpace(server)
	}

	return adapter.NewBinance(binanceAPIKey, binanceSecretKey, adapter.WithBalancer(servers...))
}

func validateAddress(addr string) bool {
	// Разделяем строку на части по символу ":"
	parts := strings.Split(addr, ":")
	if len(parts) != 2 {
		return false
	}

	ip, portStr := parts[0], parts[1]

	// Проверяем валидность IP-адреса
	if net.ParseIP(ip) == nil {
		return false
	}

	// Проверяем валидность порта
	port, err := strconv.Atoi(portStr)
	if err != nil || port <= 0 || port > 65535 {
		return false
	}

	return true
}
