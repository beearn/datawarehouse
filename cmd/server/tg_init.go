package main

import (
	"fmt"
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/notify"
	"gitlab.com/beearn/notifyfacade"
	tele "gopkg.in/telebot.v3"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func NotifyInit(ps chanpubsub.PublishSubscriber) *notifyfacade.NotifyFacade {
	token := os.Getenv("TELEGRAM_TOKEN")
	if token == "" {
		panic("TELEGRAM_TOKEN is required")
	}
	pref := tele.Settings{
		Token:  token,
		Poller: &tele.LongPoller{Timeout: 3 * time.Second},
	}
	tg, err := tele.NewBot(pref)
	if err != nil {
		log.Fatal(err)
	}

	th := notifyfacade.NewTelegramHandler(notifyfacade.WithTelegram(tg), notifyfacade.HandleWithPubSub(ps), notifyfacade.WithDefaultRecipientID(entity.GroupEazzyEarn))
	th.RegisterOnText(func(t *notifyfacade.TelegramHandler) tele.HandlerFunc {
		return func(c tele.Context) error {
			var err error
			msg := c.Text()
			if c.Chat().ID != t.DefaultRecipientID {
				return nil
			}

			if strings.Contains(msg, "percent_limit") {
				msgParts := strings.Split(msg, " ")
				if len(msgParts) < 2 {
					t.Send(entity.MessageNotify{
						Text: fmt.Sprintf("Wrong percent_limit message format: %s", msg),
					})
					return nil
				}
				var percentLimit float64
				percentLimit, err = strconv.ParseFloat(msgParts[1], 64)
				if err != nil {
					t.Send(entity.MessageNotify{
						Text: fmt.Sprintf("Wrong percent_limit message format: %s", msg),
					})
					return nil
				}
				t.PubSub.Publish("config:percent_limit:set", &entity.Message{
					Data: percentLimit,
				})
			}
			return err
		}
	})

	warehouseGroupIDRaw := os.Getenv("WAREHOUSE_GROUP_ID")
	warehouseGroupID, err := strconv.ParseInt(warehouseGroupIDRaw, 10, 64)
	if err != nil {
		panic(err)
	}

	return notifyfacade.NewNotifyFacade(
		notifyfacade.WithTitle("🏢 Data warehouse started (c) eazzy group inc."),
		notifyfacade.WithNotify(notify.NewNotify(notify.WithTelegram(tg), notify.WithDefaultRecipientID(warehouseGroupID))),
		notifyfacade.WithPubSub(ps),
		notifyfacade.WithTopics([]string{"notify"}),
		notifyfacade.WithTelegramHandler(th),
	)
}
