package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/datawarehouse/modules/warehouse"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
)

func main() {
	err := godotenv.Load()
	// .env file is required
	if err != nil {
		panic(err)
	}
	ps := chanpubsub.NewMessageBroker()

	// Notification functionality initialization
	NotifyInit(ps).Serve()

	ex := ExchangeInit()

	excludeSymbols := []string{"AIONUSDT",
		"TCTUSDT", "MIRUSDT", "NEBLUSDT", "YFIUPUSDT", "AUTOUSDT",
		"BTTCUSDT", "USTUSDT", "BNBUPUSDT", "GNOUSDT", "BTGUSDT",
		"COCOSUSDT", "USDCUSDT",
	}

	w := warehouse.NewWarehouse(
		warehouse.WithPubSub(ps),
		warehouse.WithMarket(adapter.Futures),
		warehouse.WithBinance(ex),
		warehouse.WithExcludeSymbols(excludeSymbols),
	).Init()
	go initRpc(w)

	w.Fetch()
}
