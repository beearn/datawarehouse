module gitlab.com/beearn/datawarehouse

go 1.19

require (
	github.com/joho/godotenv v1.5.1
	gitlab.com/beearn/chanpubsub v1.0.2
	gitlab.com/beearn/entity v1.0.1
	gitlab.com/beearn/exchange-client-rpc v1.0.6
	gitlab.com/beearn/notify v1.0.0
	gitlab.com/beearn/notifyfacade v1.0.1
	google.golang.org/grpc v1.58.1
	google.golang.org/protobuf v1.31.0
	gopkg.in/telebot.v3 v3.1.3
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230822172742-b8732ec3820d // indirect
)
