package worker

import (
	"context"
	"fmt"
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/datawarehouse/tools"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
	"strings"
	"time"
)

type SymbolFetch struct {
	exchange       *adapter.Binance
	timeout        time.Duration
	stream         <-chan *entity.Message
	pubSub         chanpubsub.PublishSubscriber
	excludeSymbols []string
	market         int
}

type SymbolOpt func(*SymbolFetch)

func NewSymbolFetcher(binance *adapter.Binance, opt ...SymbolOpt) *SymbolFetch {
	s := &SymbolFetch{
		exchange: binance,
		timeout:  3 * time.Second,
		stream:   make(chan *entity.Message, 100),
	}

	for _, o := range opt {
		o(s)
	}

	if s.pubSub == nil {
		panic("pubsub is required")
	}

	return s
}

// SymbolWithMarket sets market
func SymbolWithMarket(market int) SymbolOpt {
	return func(s *SymbolFetch) {
		s.market = market
	}
}

func SymbolWithPubSub(pubSub chanpubsub.PublishSubscriber) SymbolOpt {
	return func(s *SymbolFetch) {
		s.pubSub = pubSub
	}
}

func SymbolWithExcludeSymbols(exclude []string) SymbolOpt {
	return func(s *SymbolFetch) {
		s.excludeSymbols = exclude
	}
}

// Init starts fetching
func (s *SymbolFetch) Init() *SymbolFetch {
	s.stream = s.pubSub.Subscribe("symbols:get")
	return s
}

// Serve starts fetching
func (s *SymbolFetch) Fetch() {
	for {
		select {
		case <-s.stream:
			symbols, err := s.GetSymbols(s.market)
			for err != nil {
				s.pubSub.Publish("notify", &entity.Message{
					Data: entity.MessageNotify{
						Text: fmt.Sprintf("🍭 symbols error %v\n", err),
					}})
				time.Sleep(5 * time.Second)
				symbols, err = s.GetSymbols(s.market)
				if len(symbols) == 0 {
					err = fmt.Errorf("symbols not loaded")
				}
			}
			s.pubSub.Publish("notify", &entity.Message{
				Data: entity.MessageNotify{
					Text: fmt.Sprintf("🍭 symbols loaded %v\n/loaded_symbols", len(symbols)),
				}})
			s.pubSub.Publish("symbols", &entity.Message{
				Data: symbols,
			})
		}
	}
}

func (s *SymbolFetch) GetSymbols(market int) ([]string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), s.timeout)
	defer cancel()
	data, err := s.exchange.GetMarketStat(ctx, market)
	if err != nil {
		return nil, err
	}

	var symbols []string
	for _, d := range data {
		if len(d.Volume) < 5 {
			continue
		}
		if s.filter(d.Symbol, s.excludeSymbols) {
			symbols = append(symbols, d.Symbol)
		}
	}

	return symbols, nil
}

func (s *SymbolFetch) filter(symbol string, exclude []string) bool {
	return strings.HasSuffix(symbol, "USDT") && !tools.StringInSlice(symbol, exclude)
}
