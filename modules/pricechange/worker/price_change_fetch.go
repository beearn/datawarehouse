package worker

import (
	"context"
	"fmt"
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
	"time"
)

type PriceChangeFetch struct {
	exchange *adapter.Binance
	stream   chan *entity.Message
	timeout  time.Duration
	period   time.Duration
	pubSub   chanpubsub.PublishSubscriber
	market   int
}

type Opt func(*PriceChangeFetch)

func NewPriceChange(opts ...Opt) *PriceChangeFetch {
	n := &PriceChangeFetch{}

	for _, opt := range opts {
		opt(n)
	}

	if n.exchange == nil {
		panic("exchange is required")
	}

	if n.period == 0 {
		n.period = 5 * time.Second
	}

	if n.timeout == 0 {
		n.timeout = 3 * time.Second
	}

	n.stream = make(chan *entity.Message, 100)

	return n
}

func PriceChangeWithBinance(binance *adapter.Binance) Opt {
	return func(n *PriceChangeFetch) {
		n.exchange = binance
	}
}

func PriceChangeWithPubSub(pubSub chanpubsub.PublishSubscriber) Opt {
	return func(n *PriceChangeFetch) {
		n.pubSub = pubSub
	}
}

func PriceChangeWithMarket(market int) Opt {
	return func(n *PriceChangeFetch) {
		n.market = market
	}
}

func (n *PriceChangeFetch) Fetch() {
	ticker := time.NewTicker(n.period)
	for ; ; <-ticker.C {
		go n.handlePriceChange()
	}
}

// handle price change
func (n *PriceChangeFetch) handlePriceChange() {
	ctx, cancel := context.WithTimeout(context.Background(), n.timeout)
	defer cancel()

	stat, err := n.exchange.GetMarketStat(ctx, n.market)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, s := range stat {
		if len(s.Volume) < 5 {
			continue
		}
		n.pubSub.Publish("price_change:set", &entity.Message{
			Topic: "price_change:set",
			Data:  s,
		})
	}
}
