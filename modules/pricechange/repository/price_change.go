package repository

import (
	"gitlab.com/beearn/entity"
	"sync"
)

type PriceChangeStorager interface {
	Get(symbol string) *entity.PriceChangeStats
	Set(symbol string, data *entity.PriceChangeStats)
	// add list all
	List() []*entity.PriceChangeStats
}

type PriceChange struct {
	store map[string]*entity.PriceChangeStats
	sync.Mutex
}

func NewPriceChange() *PriceChange {
	return &PriceChange{
		store: make(map[string]*entity.PriceChangeStats),
	}
}

func (pcs *PriceChange) Get(symbol string) *entity.PriceChangeStats {
	pcs.Lock()
	data := pcs.store[symbol]
	pcs.Unlock()
	if data == nil {
		return &entity.PriceChangeStats{}
	}

	return data
}

func (pcs *PriceChange) Set(symbol string, data *entity.PriceChangeStats) {
	pcs.Lock()
	pcs.store[symbol] = data
	pcs.Unlock()
}

func (pcs *PriceChange) List() []*entity.PriceChangeStats {
	pcs.Lock()
	var data []*entity.PriceChangeStats
	for _, v := range pcs.store {
		data = append(data, v)
	}
	pcs.Unlock()

	return data
}
