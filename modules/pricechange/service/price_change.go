package service

import (
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/datawarehouse/modules/pricechange/repository"
	"gitlab.com/beearn/entity"
)

type PriceChange struct {
	repository repository.PriceChangeStorager
	stream     chan *entity.Message
	pubSub     chanpubsub.PublishSubscriber
	topics     []string
}

func NewPriceChange(pubSub chanpubsub.PublishSubscriber) *PriceChange {
	if pubSub == nil {
		panic("pub sub is required")
	}

	p := &PriceChange{
		pubSub: pubSub,
	}
	p.repository = repository.NewPriceChange()

	p.stream = make(chan *entity.Message, 100)

	p.topics = []string{"price_change:get", "price_change:set"}

	return p
}

// Get returns price change stats for a given symbol
func (pc *PriceChange) Get(symbol string) *entity.PriceChangeStats {
	return pc.repository.Get(symbol)
}

// Set sets price change stats for a given symbol
func (pc *PriceChange) Set(symbol string, data *entity.PriceChangeStats) {
	pc.repository.Set(symbol, data)
}

func (p *PriceChange) List() []*entity.PriceChangeStats {
	return p.repository.List()
}

// Serve syncs price change stats from messages channel
func (pc *PriceChange) Fetch() {
	for _, topic := range pc.topics {
		stream := pc.pubSub.Subscribe(topic)
		go func() {
			for m := range stream {
				pc.stream <- m
			}
		}()
	}
	go func() {
		for m := range pc.stream {
			switch m.Topic {
			case "price_change:get":
				symbol := m.Data.(string)
				priceChangeStat := pc.Get(symbol)
				pc.pubSub.Publish("price_change:res", &entity.Message{Topic: "price_change:get:response", Data: priceChangeStat})
			case "price_change:set":
				data := m.Data.(*entity.PriceChangeStats)
				pc.Set(data.Symbol, data)
			}
		}
	}()
}
