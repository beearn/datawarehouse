package warehouse

import (
	"gitlab.com/beearn/chanpubsub"
	ks "gitlab.com/beearn/datawarehouse/modules/klines/service"
	pcs "gitlab.com/beearn/datawarehouse/modules/pricechange/service"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
)

type Warehouse struct {
	klineService       *ks.KLines
	priceChangeService *pcs.PriceChange
	facadeFetcher      *FacadeFetcher
	binance            *adapter.Binance
	pubSub             chanpubsub.PublishSubscriber
	excludeSymbols     []string
	market             int
}

type ServiceFacadeOpt func(*Warehouse)

func NewWarehouse(opts ...ServiceFacadeOpt) *Warehouse {
	w := &Warehouse{}

	for _, opt := range opts {
		opt(w)
	}

	if w.pubSub == nil {
		panic("Warehouse: missing required params: pub sub")
	}

	if w.market == 0 {
		panic("Warehouse: missing required params: market")
	}

	w.facadeFetcher = NewFacadeFetcher(WithBinanceRateLimit(w.binance),
		FacadeFetchWithPubSub(w.pubSub),
		FacadeFetchWithMarket(w.market),
		WithExcludeSymbol(w.excludeSymbols))

	return w
}

func WithExcludeSymbols(exclude []string) ServiceFacadeOpt {
	return func(w *Warehouse) {
		w.excludeSymbols = exclude
	}
}

func WithBinance(binance *adapter.Binance) ServiceFacadeOpt {
	return func(w *Warehouse) {
		w.binance = binance
	}
}

func WithPubSub(pubSub chanpubsub.PublishSubscriber) ServiceFacadeOpt {
	return func(w *Warehouse) {
		w.pubSub = pubSub
	}
}

func WithMarket(market int) ServiceFacadeOpt {
	return func(w *Warehouse) {
		w.market = market
	}
}

// Init initializes the service
func (f *Warehouse) Init() *Warehouse {
	f.klineService = ks.NewKLines(
		ks.WithMarket(f.market),
		ks.WithPubSub(f.pubSub),
	)

	f.klineService.Init()
	f.facadeFetcher.Init()

	f.priceChangeService = pcs.NewPriceChange(f.pubSub)

	return f
}

// Serve starts the service
func (f *Warehouse) Fetch() {
	go f.klineService.Fetch()
	go f.priceChangeService.Fetch()
	f.facadeFetcher.Fetch()
}

func (f *Warehouse) GetDashboard(symbol string) *entity.DashboardPeriods {
	return f.klineService.GetDashboard(symbol)
}

func (f *Warehouse) GetAllDashboard() []*entity.DashboardPeriods {
	return f.klineService.GetAll()
}

func (f *Warehouse) GetDashboardsBySymbols(symbols []string) []*entity.DashboardPeriods {
	return f.klineService.GetFilteredBySymbols(symbols)
}

// GetPriceChangeBySymbol returns price change by symbol
func (f *Warehouse) GetPriceChangeBySymbol(symbol string) *entity.PriceChangeStats {
	return f.priceChangeService.Get(symbol)
}

// GetPriceChange returns all price change by symbols
func (f *Warehouse) GetPriceChange() []*entity.PriceChangeStats {
	return f.priceChangeService.List()
}

// GetSymbols returns all symbols
func (f *Warehouse) GetSymbols() []string {
	return f.klineService.GetSymbols()
}
