package warehouse

import (
	"gitlab.com/beearn/chanpubsub"
	kworker "gitlab.com/beearn/datawarehouse/modules/klines/worker"
	pworker "gitlab.com/beearn/datawarehouse/modules/pricechange/worker"
	sworker "gitlab.com/beearn/datawarehouse/modules/symbol/worker"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
	"time"
)

type FacadeFetcher struct {
	klines        *kworker.KlinesFetch
	priceChange   *pworker.PriceChangeFetch
	symbol        *sworker.SymbolFetch
	pubSub        chanpubsub.PublishSubscriber
	exchange      *adapter.Binance
	excludeSymbol []string
	market        int
}

type FacadeFetchOpt func(*FacadeFetcher)

func NewFacadeFetcher(opts ...FacadeFetchOpt) *FacadeFetcher {
	f := &FacadeFetcher{}

	for _, opt := range opts {
		opt(f)
	}

	if f.exchange == nil {
		panic("FacadeFetcher: missing required params: exchange")
	}

	if f.excludeSymbol == nil {
		f.excludeSymbol = []string{}
	}

	if f.pubSub == nil {
		panic("FacadeFetcher: missing required params: pub sub")
	}

	return f
}

// FacadeFetchWithMarket sets market
func FacadeFetchWithMarket(market int) FacadeFetchOpt {
	return func(f *FacadeFetcher) {
		f.market = market
	}
}

func FacadeFetchWithPubSub(pubSub chanpubsub.PublishSubscriber) FacadeFetchOpt {
	return func(f *FacadeFetcher) {
		f.pubSub = pubSub
	}
}

// WithBinanceRateLimit sets the exchange
func WithBinanceRateLimit(exchange *adapter.Binance) FacadeFetchOpt {
	return func(f *FacadeFetcher) {
		f.exchange = exchange
	}
}

func WithExcludeSymbol(excludeSymbol []string) FacadeFetchOpt {
	return func(f *FacadeFetcher) {
		f.excludeSymbol = excludeSymbol
	}
}

func (f *FacadeFetcher) Init() *FacadeFetcher {
	f.priceChange = pworker.NewPriceChange(pworker.PriceChangeWithBinance(f.exchange), pworker.PriceChangeWithPubSub(f.pubSub), pworker.PriceChangeWithMarket(f.market))
	f.symbol = sworker.NewSymbolFetcher(f.exchange, sworker.SymbolWithExcludeSymbols(f.excludeSymbol), sworker.SymbolWithPubSub(f.pubSub), sworker.SymbolWithMarket(f.market))
	f.klines = kworker.NewKlines(kworker.KlineWithBinance(f.exchange), kworker.KlineWithPubSub(f.pubSub), kworker.KlineWithMarket(f.market))
	return f
}

func (f *FacadeFetcher) Fetch() {
	go f.priceChange.Fetch()
	f.symbol.Init()
	f.pubSub.Publish("symbols:get", &entity.Message{})
	go f.symbol.Fetch()
	time.Sleep(10 * time.Millisecond)
	f.klines.Init()
	f.klines.Fetch()
}
