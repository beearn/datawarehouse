package repository

import (
	"gitlab.com/beearn/entity"
	"sync"
)

type DashboardStorager interface {
	Get(symbol, period string) *entity.Dashboard
	Set(symbol, period string, data *entity.Dashboard)
	List(symbol string) map[string]*entity.Dashboard
	GetAll() map[string]map[string]*entity.Dashboard
	GetSymbols() []string
}

type DashboardStorage struct {
	store map[string]map[string]*entity.Dashboard
	sync.Mutex
}

func NewDashboard() *DashboardStorage {
	return &DashboardStorage{
		store: make(map[string]map[string]*entity.Dashboard),
	}
}

func (cs *DashboardStorage) checkInit(symbol string) {
	if cs.store[symbol] == nil {
		cs.store[symbol] = make(map[string]*entity.Dashboard)
	}
}

func (cs *DashboardStorage) Get(symbol, period string) *entity.Dashboard {
	cs.Lock()
	res := cs.store[symbol][period]
	cs.Unlock()

	return res
}

func (cs *DashboardStorage) Set(symbol, period string, crypto *entity.Dashboard) {
	cs.Lock()
	cs.checkInit(symbol)
	cs.store[symbol][period] = crypto
	cs.Unlock()
}

// List - lists all cryptos for given symbol
func (cs *DashboardStorage) List(symbol string) map[string]*entity.Dashboard {
	cs.Lock()
	dashboards := make(map[string]*entity.Dashboard, 10)
	for _, dashboard := range cs.store[symbol] {
		dashboards[dashboard.Period] = dashboard
	}
	cs.Unlock()

	return dashboards
}

func (cs *DashboardStorage) GetAll() map[string]map[string]*entity.Dashboard {
	res := make(map[string]map[string]*entity.Dashboard)
	cs.Lock()
	defer cs.Unlock()
	for symbol, periods := range cs.store {
		if res[symbol] == nil {
			res[symbol] = make(map[string]*entity.Dashboard)
		}
		for period, dashboard := range periods {
			res[symbol][period] = dashboard
		}
	}

	return res
}

func (cs *DashboardStorage) GetSymbols() []string {
	cs.Lock()
	defer cs.Unlock()
	var res []string
	for symbol := range cs.store {
		res = append(res, symbol)
	}

	return res
}
