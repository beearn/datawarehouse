package service

import (
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/datawarehouse/modules/klines/repository"
	"gitlab.com/beearn/entity"
	"time"
)

const (
	DirectionDown = iota
	DirectionUp
)

type DataSyncMessage struct {
	Text string
}

type KLines struct {
	dashboardStorage repository.DashboardStorager
	stream           chan *entity.Message
	pubSub           chanpubsub.PublishSubscriber
	strategyReady    bool
	config           Config
}

type Config struct {
	topics      []string
	notifyTopic string
	timeout     time.Duration
	market      int
}

type Opts func(*KLines)

func NewKLines(opts ...Opts) *KLines {
	cr := &KLines{}

	for _, opt := range opts {
		opt(cr)
	}

	if cr.config.timeout == 0 {
		cr.config.timeout = 5 * time.Second
	}

	if cr.pubSub == nil {
		panic("pub sub is required")
	}

	if cr.config.topics == nil {
		cr.config.topics = []string{"klines", "bot_ready"}
	}

	if cr.stream == nil {
		cr.stream = make(chan *entity.Message, 100)
	}

	if cr.config.notifyTopic == "" {
		cr.config.notifyTopic = "notify"
	}

	cr.dashboardStorage = repository.NewDashboard()

	return cr
}

// WithNotifyTopic sets notify topic
func WithNotifyTopic(notifyTopic string) Opts {
	return func(cr *KLines) {
		cr.config.notifyTopic = notifyTopic
	}
}

// WithPubSub sets pub sub
func WithPubSub(pubSub chanpubsub.PublishSubscriber) Opts {
	return func(cr *KLines) {
		cr.pubSub = pubSub
	}
}

// WithMarket sets market
func WithMarket(market int) Opts {
	return func(cr *KLines) {
		cr.config.market = market
	}
}

func (d *KLines) sendNotify(text, format string) {
	d.pubSub.Publish(d.config.notifyTopic, &entity.Message{Topic: d.config.notifyTopic, Data: entity.MessageNotify{
		Text:   text,
		Format: format,
	}})
}

func (d *KLines) Init() *KLines {
	for _, topic := range d.config.topics {
		c := d.pubSub.Subscribe(topic)
		go func() {
			for {
				select {
				case msg := <-c:
					d.stream <- msg
				}
			}
		}()
	}

	return d
}

// Serve starts the service
func (d *KLines) Fetch() {
	for msg := range d.stream {
		switch msg.Topic {
		case "klines":
			klineStat := msg.Data.(entity.KlineStats)
			d.SaveSymbol(klineStat.Klines, klineStat.Symbol, klineStat.Period)
			if klineStat.Period == "1m" && d.strategyReady {

			}
		case "dashboard:get_symbol":
			symbol := msg.Data.(string)
			dashboard := d.dashboardStorage.List(symbol)
			d.pubSub.Publish("dashboard:symbol_res", &entity.Message{Topic: "dashboard", Data: &entity.DashboardPeriods{
				Symbol:  symbol,
				Periods: dashboard,
			}})
		case "bot_ready":
			d.strategyReady = true
		}
	}
}

func (d *KLines) SaveSymbol(klines []*entity.Kline, symbol, period string) {
	dashboardElem := &entity.Dashboard{
		Symbol: symbol,
		Klines: klines,
		Market: d.config.market,
		Period: period,
	}

	d.dashboardStorage.Set(symbol, period, dashboardElem)
}

func (d *KLines) GetDashboard(symbol string) *entity.DashboardPeriods {
	dMap := d.dashboardStorage.List(symbol)

	return &entity.DashboardPeriods{
		Symbol:  symbol,
		Periods: dMap,
	}
}

func (d *KLines) GetAll() []*entity.DashboardPeriods {
	symbolPeriodDashboards := d.dashboardStorage.GetAll()
	var res []*entity.DashboardPeriods
	for symbol, periods := range symbolPeriodDashboards {
		res = append(res, &entity.DashboardPeriods{
			Symbol:  symbol,
			Periods: periods,
		})
	}

	return res
}

// GetSymbols returns all symbols
func (d *KLines) GetSymbols() []string {
	return d.dashboardStorage.GetSymbols()
}

func (d *KLines) GetFilteredBySymbols(symbols []string) []*entity.DashboardPeriods {
	var res []*entity.DashboardPeriods
	for _, symbol := range symbols {
		res = append(res, d.GetDashboard(symbol))
	}

	return res
}
