package worker

import (
	"context"
	"fmt"
	"gitlab.com/beearn/chanpubsub"
	"gitlab.com/beearn/entity"
	ecr "gitlab.com/beearn/exchange-client-rpc"
	"gitlab.com/beearn/exchange-client-rpc/rpc/adapter"
	"strings"
	"sync"
	"time"
)

type KlinesFetch struct {
	exchange *adapter.Binance
	stream   chan *entity.Message
	pubSub   chanpubsub.PublishSubscriber
	config   Config
}

type Config struct {
	errRecipientID int64
	periods        []string
	symbols        []string
	topics         []string
	timeout        time.Duration
	market         int
}

type KlineOpt func(*KlinesFetch)

func NewKlines(opts ...KlineOpt) *KlinesFetch {
	n := &KlinesFetch{}

	for _, opt := range opts {
		opt(n)
	}

	if n.exchange == nil {
		panic("exchange is required")
	}

	if n.config.periods == nil {
		n.config.periods = []string{"1m", "15m", "1h", "4h", "12h", "1d", "1w", "1M"}
	}

	if n.config.timeout == 0 {
		n.config.timeout = 8 * time.Second
	}

	if n.pubSub == nil {
		panic("pubsub is required")
	}

	if n.config.topics == nil {
		n.config.topics = []string{"symbols"}
	}

	if n.stream == nil {
		n.stream = make(chan *entity.Message, 100)
	}

	for _, topic := range n.config.topics {
		c := n.pubSub.Subscribe(topic)
		go func() {
			for {
				select {
				case v := <-c:
					n.stream <- v
				}
			}
		}()
	}

	return n
}

func KlineWithSymbols(symbols []string) KlineOpt {
	return func(n *KlinesFetch) {
		n.config.symbols = symbols
	}
}

func KlineWithBinance(binance *adapter.Binance) KlineOpt {
	return func(n *KlinesFetch) {
		n.exchange = binance
	}
}

func KlineWithPeriod(period []string) KlineOpt {
	return func(n *KlinesFetch) {
		n.config.periods = period
	}
}

func KlineWithTimeout(timeout time.Duration) KlineOpt {
	return func(n *KlinesFetch) {
		n.config.timeout = timeout
	}
}

func KlineWithMarket(market int) KlineOpt {
	return func(n *KlinesFetch) {
		n.config.market = market
	}
}

// KlineWithPubSub
func KlineWithPubSub(pubSub chanpubsub.PublishSubscriber) KlineOpt {
	return func(n *KlinesFetch) {
		n.pubSub = pubSub
	}
}

func (kf *KlinesFetch) sendNotify(text string) {
	kf.pubSub.Publish("notify", &entity.Message{
		Data: entity.MessageNotify{
			Text: text,
		}})
}

func (kf *KlinesFetch) sendError(text string) {
	kf.pubSub.Publish("notify", &entity.Message{
		Data: entity.MessageNotify{
			Text:        text,
			RecipientID: kf.config.errRecipientID,
		}})
}

func (kf *KlinesFetch) Init() *KlinesFetch {
	initRun := make(chan struct{})
	for {
		select {
		case msg := <-kf.stream:
			switch msg.Topic {
			case "symbols":
				kf.config.symbols = msg.Data.([]string)
			}
			close(initRun)
		case <-initRun:
			kf.preparePeriods()
			go kf.ProcessMessage()
			return kf
		}
	}
}

func (kf *KlinesFetch) ProcessMessage() {
	for {
		select {
		case msg := <-kf.stream:
			switch msg.Topic {
			case "klines:fetch":
				parts := strings.Split(msg.Data.(string), ":")
				if len(parts) != 2 {
					fmt.Println("invalid message", msg.Data)
					kf.pubSub.Publish("notify", &entity.Message{
						Data: entity.MessageNotify{
							Text: "klines_fetch invalid message " + msg.Data.(string),
						},
					})
					continue
				}
				go kf.processSymbol(parts[0], parts[1])
			}
		}
	}
}

func (kf *KlinesFetch) preparePeriods() {
	trackingPeriods := []string{"15m", "1h", "4h", "12h", "1d", "1w", "1M"}
	var wg sync.WaitGroup
	kf.sendNotify("🍑 start init data")
	fmt.Println("start init data")
	timeout := kf.config.timeout
	kf.config.timeout = 13 * time.Second

	kf.sendNotify("☃️ wait init data periods: " + strings.Join(trackingPeriods, " "))
	fmt.Println("wait init data")
	var throttle time.Duration = 200
	for _, period := range trackingPeriods {
		for _, symbol := range kf.config.symbols {
			time.Sleep(throttle * time.Millisecond)
			wg.Add(1)
			go func(symbol, period string) {
				defer wg.Done()
				kf.ProcessRetry(symbol, period)
			}(symbol, period)
		}
	}
	wg.Wait()

	wg = sync.WaitGroup{}
	kf.sendNotify("🙇‍♂️️ wait a minute")
	for _, symbol := range kf.config.symbols {
		time.Sleep(throttle * time.Millisecond) // TODO: move to config
		wg.Add(1)
		go func(symbol string) {
			defer wg.Done()
			kf.ProcessRetry(symbol, "1m")
		}(symbol)
	}
	wg.Wait()

	kf.config.timeout = timeout
}

func (kf *KlinesFetch) Fetch() {
	var wg sync.WaitGroup
	wg.Add(1)
	for _, period := range kf.config.periods {
		time.Sleep(15 * time.Second)
		go kf.ProcessKlines(period)
	}
	kf.pubSub.Publish("bot_ready", &entity.Message{})
	kf.sendNotify("☑️ init data complete")
	kf.sendNotify("⚡️ bot is ready")
	wg.Wait()
}

func (kf *KlinesFetch) processSymbol(symbol, period string) (entity.KlineStats, error) {
	ctx, cancel := context.WithTimeout(context.Background(), kf.config.timeout)
	defer cancel()
	klinesRaw, err := kf.exchange.Klines(ctx, symbol, period, kf.config.market)
	klines := entity.KlineStats{
		Symbol: symbol,
		Period: period,
		Klines: klinesRaw,
	}

	if err != nil {
		fmt.Printf("Failed to process symbol %s: %s\n", symbol, err)
		kf.log("⚠️ error", period, err)
		return entity.KlineStats{}, err
	}

	if len(klines.Klines) == 0 {
		fmt.Println("klines is empty", symbol, period)
		kf.sendNotify(fmt.Sprintf("klines is empty %s %s", symbol, period))
		return entity.KlineStats{}, err
	}

	return klines, nil
}

func (kf *KlinesFetch) ProcessRetry(symbol, period string) {
	msgRateLimitExceeded := fmt.Sprintf("⚠️ rate limit exceeded sleep 1 minute, period")
	msgContinue := fmt.Sprintf("♻️ continue after rate limit exceeded, period")
	err := fmt.Errorf("process retry started")
	var klines entity.KlineStats
	var retryCount int
	for err != nil {
		if retryCount > 3 {
			break
		}
		klines, err = kf.processSymbol(symbol, period)
		if err != nil {
			fmt.Printf("Failed to process symbol %s: %s\n", symbol, err)
			if strings.Contains(err.Error(), "code=-1003") {
				kf.log(msgRateLimitExceeded, period, err)
			} else {
				kf.log("⚠️ error", period, err)
			}

			time.Sleep(15 * time.Second)
			kf.log(msgContinue, period, nil)
		}
		retryCount++
	}

	if len(klines.Klines) == 0 {
		fmt.Println("klines is empty", symbol, period)
		kf.sendNotify(fmt.Sprintf("klines is empty %s %s", symbol, period))
		return
	}

	kf.pubSub.Publish("klines", &entity.Message{
		Topic: "klines",
		Data:  klines,
	})
}

func (kf *KlinesFetch) ProcessKlines(period string) {
	for _, symbol := range kf.config.symbols {
		time.Sleep(300 * time.Millisecond)
		go func(symbol, period string) {
			d := ecr.PeriodToDuration(period) / 3
			if period == "1m" {
				d = 30 * time.Second
			}
			ticker := time.NewTicker(d)
			for {
				select {
				case <-ticker.C:
					fmt.Println("process symbol", symbol, period)
					kf.ProcessRetry(symbol, period)
					fmt.Println("process symbol done", symbol, period)
				}
			}
		}(symbol, period)
	}
}

// log error
func (kf *KlinesFetch) log(message, period string, err error) {
	errStr := ""
	if err != nil {
		errStr = err.Error()
	}
	kf.sendError(fmt.Sprintf("%s %s %s", message, period, errStr))
	fmt.Printf("%s %s %s\n", message, period, errStr)
}
