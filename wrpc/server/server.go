package server

import (
	"context"
	"gitlab.com/beearn/datawarehouse/modules/warehouse"
	"gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/entity"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type DataWarehouseServer struct {
	wrpc.UnimplementedWarehouseServer
	warehouse *warehouse.Warehouse
}

func NewDataWarehouseServer(warehouse *warehouse.Warehouse) *DataWarehouseServer {
	return &DataWarehouseServer{warehouse: warehouse}
}

func convertDashboard(dashboard *entity.DashboardPeriods) *wrpc.DashboardPeriods {
	periods := make(map[string]*wrpc.Dashboard, len(dashboard.Periods))
	for period, periodDashboard := range dashboard.Periods {
		var klines []*wrpc.Kline
		for _, kline := range periodDashboard.Klines {
			klines = append(klines, &wrpc.Kline{
				OpenTime:                 kline.OpenTime,
				Open:                     kline.Open,
				High:                     kline.High,
				Low:                      kline.Low,
				Close:                    kline.Close,
				Volume:                   kline.Volume,
				CloseTime:                kline.CloseTime,
				QuoteAssetVolume:         kline.QuoteAssetVolume,
				TradeNum:                 kline.TradeNum,
				TakerBuyBaseAssetVolume:  kline.TakerBuyBaseAssetVolume,
				TakerBuyQuoteAssetVolume: kline.TakerBuyQuoteAssetVolume,
			})
		}

		periods[period] = &wrpc.Dashboard{
			Symbol: periodDashboard.Symbol,
			Period: periodDashboard.Period,
			Market: int32(periodDashboard.Market),
			Klines: klines,
		}
	}
	return &wrpc.DashboardPeriods{
		Symbol:  dashboard.Symbol,
		Periods: periods,
	}
}

func (d *DataWarehouseServer) GetDashboard(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.DashboardPeriods, error) {
	dashboard := d.warehouse.GetDashboard(in.GetSymbol())
	if dashboard == nil {
		return nil, status.Error(codes.NotFound, "Dashboard not found")
	}

	periods := make(map[string]*wrpc.Dashboard, len(dashboard.Periods))
	for period, periodDashboard := range dashboard.Periods {
		var klines []*wrpc.Kline
		for _, kline := range periodDashboard.Klines {
			klines = append(klines, &wrpc.Kline{
				OpenTime:                 kline.OpenTime,
				Open:                     kline.Open,
				High:                     kline.High,
				Low:                      kline.Low,
				Close:                    kline.Close,
				Volume:                   kline.Volume,
				CloseTime:                kline.CloseTime,
				QuoteAssetVolume:         kline.QuoteAssetVolume,
				TradeNum:                 kline.TradeNum,
				TakerBuyBaseAssetVolume:  kline.TakerBuyBaseAssetVolume,
				TakerBuyQuoteAssetVolume: kline.TakerBuyQuoteAssetVolume,
			})
		}

		periods[period] = &wrpc.Dashboard{
			Symbol: periodDashboard.Symbol,
			Period: periodDashboard.Period,
			Market: int32(periodDashboard.Market),
			Klines: klines,
		}
	}
	return &wrpc.DashboardPeriods{
		Symbol:  in.Symbol,
		Periods: periods,
	}, nil
}

func (d *DataWarehouseServer) GetAllDashboard(ctx context.Context, empty *emptypb.Empty) (*wrpc.GetAllDashboardOut, error) {
	dashboards := d.warehouse.GetAllDashboard()

	var res []*wrpc.DashboardPeriods
	for _, dashboard := range dashboards {
		res = append(res, convertDashboard(dashboard))
	}
	return &wrpc.GetAllDashboardOut{Dashboard: res}, nil
}

func (d *DataWarehouseServer) GetDashboardsBySymbols(ctx context.Context, in *wrpc.GetDashboardsBySymbolsIn) (*wrpc.GetDashboardsBySymbolsOut, error) {
	dashboards := d.warehouse.GetDashboardsBySymbols(in.GetSymbols())
	var res []*wrpc.DashboardPeriods
	for _, dashboard := range dashboards {
		res = append(res, convertDashboard(dashboard))
	}
	return &wrpc.GetDashboardsBySymbolsOut{Dashboard: res}, nil
}

func (d *DataWarehouseServer) GetPriceChangeBySymbol(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.PriceChangeStats, error) {
	priceChange := d.warehouse.GetPriceChangeBySymbol(in.GetSymbol())
	if priceChange == nil {
		return nil, status.Error(codes.NotFound, "Price Change not found")
	}
	return convertPriceChangeStats(priceChange), nil
}

func (d *DataWarehouseServer) GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*wrpc.GetPriceChangeOut, error) {
	priceChanges := d.warehouse.GetPriceChange()
	var res []*wrpc.PriceChangeStats
	for _, priceChange := range priceChanges {
		res = append(res, convertPriceChangeStats(priceChange))
	}
	return &wrpc.GetPriceChangeOut{PriceChange: res}, nil
}

func (d *DataWarehouseServer) GetSymbols(ctx context.Context, empty *emptypb.Empty) (*wrpc.GetSymbolsOut, error) {
	symbols := d.warehouse.GetSymbols()
	return &wrpc.GetSymbolsOut{Symbols: symbols}, nil
}

func convertPriceChangeStats(priceChangeStats *entity.PriceChangeStats) *wrpc.PriceChangeStats {
	return &wrpc.PriceChangeStats{
		Symbol:             priceChangeStats.Symbol,
		PriceChange:        priceChangeStats.PriceChange,
		PriceChangePercent: priceChangeStats.PriceChangePercent,
		WeightedAvgPrice:   priceChangeStats.WeightedAvgPrice,
		PrevClosePrice:     priceChangeStats.PrevClosePrice,
		LastPrice:          priceChangeStats.LastPrice,
		LastQty:            priceChangeStats.LastQty,
		BidPrice:           priceChangeStats.BidPrice,
		AskPrice:           priceChangeStats.AskPrice,
		OpenPrice:          priceChangeStats.OpenPrice,
		HighPrice:          priceChangeStats.HighPrice,
		LowPrice:           priceChangeStats.LowPrice,
		Volume:             priceChangeStats.Volume,
		QuoteVolume:        priceChangeStats.QuoteVolume,
		OpenTime:           priceChangeStats.OpenTime,
		CloseTime:          priceChangeStats.CloseTime,
		FirstId:            priceChangeStats.FirstID,
		LastId:             priceChangeStats.LastID,
		Count:              priceChangeStats.Count,
	}
}
