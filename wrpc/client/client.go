package client

import (
	"context"
	"gitlab.com/beearn/datawarehouse/wrpc"

	"gitlab.com/beearn/entity"
	"google.golang.org/grpc"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	"log"
	"time"
)

type WarehouseClient struct {
	rpc     wrpc.WarehouseClient
	timeout time.Duration
	Address string
}

func NewWarehouseClient(addr string, timeout time.Duration) *WarehouseClient {
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(1024*1024*1024)))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	c := wrpc.NewWarehouseClient(conn)
	if timeout == 0 {
		timeout = 1 * time.Second
	}

	return &WarehouseClient{
		rpc:     c,
		Address: addr,
		timeout: timeout,
	}
}

func (w *WarehouseClient) getTimeoutContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), w.timeout)
}

func (w *WarehouseClient) GetDashboard(symbol string) *entity.DashboardPeriods {
	in := &wrpc.GetDashboardIn{
		Symbol: symbol,
	}

	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetDashboard(ctx, in)
	if err != nil {
		log.Printf("error calling GetDashboard: %v", err)
		return nil
	}

	return convertDashboardPeriods(out)
}

func (w *WarehouseClient) GetAllDashboard() []*entity.DashboardPeriods {
	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetAllDashboard(ctx, &emptypb.Empty{})
	if err != nil {
		log.Printf("error calling GetAllDashboard: %v", err)
		return nil
	}

	var dashboards []*entity.DashboardPeriods
	for _, dashboard := range out.Dashboard {
		dashboards = append(dashboards, convertDashboardPeriods(dashboard))
	}
	return dashboards
}

func (w *WarehouseClient) GetDashboardsBySymbols(symbols []string) []*entity.DashboardPeriods {
	in := &wrpc.GetDashboardsBySymbolsIn{
		Symbols: symbols,
	}

	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetDashboardsBySymbols(ctx, in)
	if err != nil {
		log.Printf("error calling GetDashboardsBySymbols: %v", err)
		return nil
	}

	var dashboards []*entity.DashboardPeriods
	for _, dashboard := range out.Dashboard {
		dashboards = append(dashboards, convertDashboardPeriods(dashboard))
	}
	return dashboards
}

func (w *WarehouseClient) GetPriceChangeBySymbol(symbol string) *entity.PriceChangeStats {
	in := &wrpc.GetDashboardIn{
		Symbol: symbol,
	}

	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetPriceChangeBySymbol(ctx, in)
	if err != nil {
		log.Printf("error calling GetPriceChangeBySymbol: %v", err)
		return nil
	}

	return convertPriceChangeStats(out)
}

func (w *WarehouseClient) GetSymbols() []string {
	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetSymbols(ctx, &emptypb.Empty{})
	if err != nil {
		log.Printf("error calling GetSymbols: %v", err)
		return nil
	}

	return out.Symbols
}

func (w *WarehouseClient) GetPriceChange() []*entity.PriceChangeStats {
	ctx, cancel := w.getTimeoutContext()
	defer cancel()
	out, err := w.rpc.GetPriceChange(ctx, &emptypb.Empty{})
	if err != nil {
		log.Printf("error calling GetPriceChange: %v", err)
		return nil
	}

	var stats []*entity.PriceChangeStats
	for _, stat := range out.PriceChange {
		stats = append(stats, convertPriceChangeStats(stat))
	}
	return stats
}

func convertDashboardPeriods(dashboardPeriods *wrpc.DashboardPeriods) *entity.DashboardPeriods {
	periods := make(map[string]*entity.Dashboard, len(dashboardPeriods.Periods))
	for period, dashboard := range dashboardPeriods.Periods {
		var klines []*entity.Kline
		for _, kline := range dashboard.Klines {
			klines = append(klines, &entity.Kline{
				OpenTime:                 kline.OpenTime,
				Open:                     kline.Open,
				High:                     kline.High,
				Low:                      kline.Low,
				Close:                    kline.Close,
				Volume:                   kline.Volume,
				CloseTime:                kline.CloseTime,
				QuoteAssetVolume:         kline.QuoteAssetVolume,
				TradeNum:                 kline.TradeNum,
				TakerBuyBaseAssetVolume:  kline.TakerBuyBaseAssetVolume,
				TakerBuyQuoteAssetVolume: kline.TakerBuyQuoteAssetVolume,
			})
		}

		periods[period] = &entity.Dashboard{
			Symbol: dashboard.Symbol,
			Period: dashboard.Period,
			Market: int(dashboard.Market),
			Klines: klines,
		}
	}

	return &entity.DashboardPeriods{
		Symbol:  dashboardPeriods.Symbol,
		Periods: periods,
	}
}

func convertPriceChangeStats(priceChangeStats *wrpc.PriceChangeStats) *entity.PriceChangeStats {
	return &entity.PriceChangeStats{
		Symbol:             priceChangeStats.Symbol,
		PriceChange:        priceChangeStats.PriceChange,
		PriceChangePercent: priceChangeStats.PriceChangePercent,
		WeightedAvgPrice:   priceChangeStats.WeightedAvgPrice,
		PrevClosePrice:     priceChangeStats.PrevClosePrice,
		LastPrice:          priceChangeStats.LastPrice,
		LastQty:            priceChangeStats.LastQty,
		BidPrice:           priceChangeStats.BidPrice,
		BidQty:             priceChangeStats.BidQty,
		AskPrice:           priceChangeStats.AskPrice,
		AskQty:             priceChangeStats.AskQty,
		OpenPrice:          priceChangeStats.OpenPrice,
		HighPrice:          priceChangeStats.HighPrice,
		LowPrice:           priceChangeStats.LowPrice,
		Volume:             priceChangeStats.Volume,
		QuoteVolume:        priceChangeStats.QuoteVolume,
		OpenTime:           priceChangeStats.OpenTime,
		CloseTime:          priceChangeStats.CloseTime,
		FirstID:            priceChangeStats.FirstId,
		LastID:             priceChangeStats.LastId,
		Count:              priceChangeStats.Count,
	}
}
